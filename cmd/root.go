package cmd

import (
	"context"
	"fmt"
	"github.com/chromedp/chromedp"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"gitlab.com/iotssl/luftdaten_screenshot_tool/actions"
	"log"
	"os"
	"sync"
)

var FolderPath string
var Headless bool

func init() {
	rootCmd.Flags().StringVarP(&FolderPath, "output", "o", "./result", "Folder where the images should go to")
	rootCmd.Flags().BoolVarP(&Headless, "headless", "", false, "Use Headless chrome")
}

var rootCmd = &cobra.Command{
	Use:     "luftdaten_screenshot_tool",
	Short:   "LuftdatenImage is a simple luftdaten.info web screenshoter",
	Long:    `A simple headless screenshot Tool for luftdaten.info build with golang and Google Chrome`,
	Version: "0.2.0",
	RunE:    Run,
}

func Run(cmd *cobra.Command, args []string) error {
	var err error

	// create context
	ctxt, cancel := context.WithCancel(context.Background())
	defer cancel()

	// create chrome instance
	pool, err := chromedp.NewPool()

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		err := takeScreenshot(ctxt, &wg, pool)
		if err != nil {
			fmt.Println(err)
		}
	}()

	// wait for to finish
	wg.Wait()

	err = pool.Shutdown()
	if err != nil {
		return errors.WithMessage(err, "Failed to call Shutdown")
	}
	log.Println("waiting for headless_shell to exit...")
	// wait for chrome to finish
	// TODO: Hope that Wait can be used again in the future
	/*err = c.Wait()
	if err != nil {
		return err
	}*/
	return nil
}

func takeScreenshot(ctxt context.Context, wg *sync.WaitGroup, pool *chromedp.Pool) error {
	defer wg.Done()

	// allocate
	c, err := pool.Allocate(ctxt)
	if err != nil {
		return errors.WithMessage(err, "Failed to call Allocate")
	}
	defer c.Release()

	// run tasks
	err = c.Run(ctxt, actions.Screenshot(`http://deutschland.maps.luftdaten.info/`, FolderPath))
	if err != nil {
		return errors.WithMessage(err, "Failed to call Run")
	}
	return nil
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
