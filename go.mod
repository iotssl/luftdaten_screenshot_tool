module gitlab.com/iotssl/luftdaten_screenshot_tool

require (
	github.com/chromedp/cdproto v0.0.0-20181201113921-96cd0ad398e9
	github.com/chromedp/chromedp v0.0.0-20181201115416-98d4b0de6e1b
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/pkg/errors v0.8.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.3 // indirect
)
