package main

import (
	"gitlab.com/iotssl/luftdaten_screenshot_tool/cmd"
)

func main() {
	cmd.Execute()
}
