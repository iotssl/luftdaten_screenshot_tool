package actions

import (
	"context"
	"fmt"
	"github.com/chromedp/cdproto/cdp"
	cdr "github.com/chromedp/cdproto/runtime"
	"github.com/chromedp/chromedp"
	"github.com/pkg/errors"
	"io/ioutil"
	"os"
	"time"
)

func Screenshot(urlstr, folderPath string) chromedp.Tasks {
	var buf []byte
	var res *cdr.RemoteObject

	currentTime := time.Now().Local()

	var (
		addTimeandDateScript = "" +
			"var element = document.createElement('div');" +
			"element.innerHTML = 'Current Time: " + currentTime.Format("02.01.2006 15:04:05") + "';" +
			"element.style.position = 'absolute';" +
			"element.style.top = '0';" +
			"element.style.right = '0';" +
			"element.style.zIndex = '500';" +
			"element.style.fontSize = '25px';" +
			"element.style.fontWeight = 'bold';" +
			"element.className = 'timeDate';" +
			"document.getElementsByClassName('map')[0].appendChild(element);"
	)

	return chromedp.Tasks{
		chromedp.Navigate(urlstr),
		chromedp.Sleep(2 * time.Second),
		chromedp.WaitVisible(`.map`, chromedp.ByQuery),
		chromedp.EvaluateAsDevTools(addTimeandDateScript, &res),
		chromedp.WaitVisible(`.timeDate`, chromedp.ByQuery),
		chromedp.Screenshot(`.map`, &buf, chromedp.ByQuery),
		chromedp.ActionFunc(func(context.Context, cdp.Executor) error {
			if _, err := os.Stat(folderPath); os.IsNotExist(err) {
				err := os.Mkdir(folderPath, os.ModePerm)
				if err != nil {
					return errors.WithMessage(err, "Failed to call Shutdown")
				}
			}
			files, _ := ioutil.ReadDir(folderPath)
			count := len(files) + 1
			filename := fmt.Sprintf("%s/map%03d.png", folderPath, count)
			return ioutil.WriteFile(filename, buf, 0644)
		}),
	}
}
