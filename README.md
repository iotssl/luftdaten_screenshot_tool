# Luftdaten Screenshot Tool

## Instalation

1. Installiere golang
2. Clone diese repo mit `go get -u -v gitlab.com/iotssl/luftdaten_screenshot_tool`
3. Installiere die nötigen Abhängigkeiten: `Chrome`
4. Folge dem Abschnitt Benutzung

## Benutzung

Die Benutzung ist simpel. Das Programm (binary heisst `luftdaten_screenshot_tool`) öffnet chrome und macht einen screenshot in den ordner result mit aufsteigender Nummerierung bei jedem Start. Das ganze am besten in einen Cronjob machen und am ende dann zu ein gif umwandeln.

Mithilfe der option `--output "./result"` kann zudem ein ausgabe ordner definiert werden.

Die Option `--headless` unterbindet das sichtbare öffnen des Browsers.

Die Option `--version` gibt die aktuelle Version wieder.

## GIF mit ffmpeg generieren

Der folgende Command generiert ein gif mit einer fps von 10
```
ffmpeg -f image2 -framerate 10 -i <OUTPUT ORDNER>/map%03d.png out.gif
```
Oder noch etwas besser:
```
ffmpeg -f image2 -framerate 10 -i map%03d.png -b:v 500k -vcodec libvpx <OUTPUT ORDNER>/tag.webm
```